<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/user', name: 'app_user')]
    public function index(UsersRepository $usersRepository): Response
    {
        $data = $usersRepository ->findAll();
        //dd($data);
        return $this->render('user/index.html.twig', [
            'list' => $data,
        ]);
    }
}
