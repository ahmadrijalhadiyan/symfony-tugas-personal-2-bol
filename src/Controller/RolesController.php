<?php

namespace App\Controller;

use App\Entity\Roles;
use App\Repository\RolesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RolesController extends AbstractController
{
    #[Route('/roles', name: 'app_roles')]
    public function index(RolesRepository $rolesRepository): Response
    {
        $data = $rolesRepository->findAll();
        //dd($data);
        return $this->render('roles/index.html.twig', [
            'list' => $data,
        ]);
    }
}
