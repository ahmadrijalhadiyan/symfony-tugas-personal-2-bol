<?php

namespace App\Entity;

use App\Repository\RolesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RolesRepository::class)]
class Roles
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $role_access_id = null;

    #[ORM\Column(length: 15, nullable: true)]
    private ?string $keterangan = null;

   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoleAccessId(): ?int
    {
        return $this->role_access_id;
    }

    public function setRoleAccessId(?int $role_access_id): self
    {
        $this->role_access_id = $role_access_id;

        return $this;
    }

    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }
}
