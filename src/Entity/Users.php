<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UsersRepository::class)]
class Users
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $user_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $role_access_id = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $nama_depan = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $nama_belakang = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $alamat = null;

    #[ORM\Column(length: 15, nullable: true)]
    private ?string $jabatan = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(?int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getRoleAccessId(): ?int
    {
        return $this->role_access_id;
    }

    public function setRoleAccessId(?int $role_access_id): self
    {
        $this->role_access_id = $role_access_id;

        return $this;
    }

    public function getNamaDepan(): ?string
    {
        return $this->nama_depan;
    }

    public function setNamaDepan(?string $nama_depan): self
    {
        $this->nama_depan = $nama_depan;

        return $this;
    }

    public function getNamaBelakang(): ?string
    {
        return $this->nama_belakang;
    }

    public function setNamaBelakang(?string $nama_belakang): self
    {
        $this->nama_belakang = $nama_belakang;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAlamat(): ?string
    {
        return $this->alamat;
    }

    public function setAlamat(?string $alamat): self
    {
        $this->alamat = $alamat;

        return $this;
    }

    public function getJabatan(): ?string
    {
        return $this->jabatan;
    }

    public function setJabatan(?string $jabatan): self
    {
        $this->jabatan = $jabatan;

        return $this;
    }
}
