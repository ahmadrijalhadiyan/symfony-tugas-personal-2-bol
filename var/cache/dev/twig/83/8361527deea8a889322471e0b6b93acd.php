<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* roles/index.html.twig */
class __TwigTemplate_ee17cac8dd4f7c06e66bb386a78b26af extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "roles/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "roles/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Dasboard DIM";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

<body class=\"hold-transition sidebar-mini\">
    <!-- Site wrapper -->
    <div class=\"wrapper\">
        <!-- Navbar -->
        <nav class=\"main-header navbar navbar-expand navbar-white navbar-light\">
            <!-- Left navbar links -->
            <ul class=\"navbar-nav\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\" role=\"button\"><i class=\"fas fa-bars\"></i></a>
                </li>
                <li class=\"nav-item d-none d-sm-inline-block\">
                    <a href=\"../../index3.html\" class=\"nav-link\">Home</a>
                </li>
                <li class=\"nav-item d-none d-sm-inline-block\">
                    <a href=\"#\" class=\"nav-link\">Contact</a>
                </li>
            </ul>

            <!-- Right navbar links -->
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class=\"main-sidebar sidebar-dark-primary elevation-4\">

            <!-- Sidebar -->
            <div class=\"sidebar\">
                <!-- Sidebar user (optional) -->
                <div class=\"user-panel mt-3 pb-3 mb-3 d-flex\">

                </div>

                <!-- SidebarSearch Form -->
                <div class=\"form-inline\">
                    <div class=\"input-group\" data-widget=\"sidebar-search\">
                        <input class=\"form-control form-control-sidebar\" type=\"search\" placeholder=\"Search\"
                            aria-label=\"Search\">
                        <div class=\"input-group-append\">
                            <button class=\"btn btn-sidebar\">
                                <i class=\"fas fa-search fa-fw\"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class=\"mt-2\">
                    <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\"
                        data-accordion=\"false\">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class=\"nav-item\">
                            <a href=\"#\" class=\"nav-link\">
                                <i class=\"nav-icon fas fa-tachometer-alt\"></i>
                                <p>
                                    Konfigurasi
                                    <i class=\"right fas fa-angle-left\"></i>
                                </p>
                            </a>
                            <ul class=\"nav nav-treeview\">
                                <li class=\"nav-item\">
                                    <a href=\"";
        // line 69
        echo "/roles";
        echo "\" class=\"nav-link\">
                                        <i class=\"far fa-circle nav-icon\"></i>
                                        <p>Roles Pengguna</p>
                                    </a>
                                </li>
                                <li class=\"nav-item\">
                                    <a href=\"";
        // line 75
        echo "user";
        echo "\" class=\"nav-link\">
                                        <i class=\"far fa-circle nav-icon\"></i>
                                        <p>Pengguna</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class=\"content-wrapper\">
            <!-- Content Header (Page header) -->
            <section class=\"content-header\">
                <div class=\"container-fluid\">
                    <div class=\"row mb-2\">
                        <div class=\"col-sm-6\">
                            <h1>Roles Pengguna</h1>
                        </div>
                        <div class=\"col-sm-6\">
                            <ol class=\"breadcrumb float-sm-right\">
                                <li class=\"breadcrumb-item\">Roles</li>
                                <li class=\"breadcrumb-item active\">Roles Pengguna</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
    <section class=\"content\">
      <div class=\"container-fluid\">
        <div class=\"row\">
          <div class=\"col-12\">
            <div class=\"card\">
              <div class=\"card-header\">
                <h3 class=\"card-title\">DataTable with minimal features & hover style</h3>
              </div>
              <!-- /.card-header -->
              <div class=\"card-body\">
                <table id=\"example2\" class=\"table table-bordered table-hover\">
                  <thead>
                  <tr>
                    <th>ID Roles Access</th>
                    <th>Keterangan</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                    ";
        // line 129
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) || array_key_exists("list", $context) ? $context["list"] : (function () { throw new RuntimeError('Variable "list" does not exist.', 129, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 130
            echo "                  <tr>
                    <td>";
            // line 131
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "id", [], "any", false, false, false, 131), "html", null, true);
            echo "</td>
                    <td>";
            // line 132
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["data"], "keterangan", [], "any", false, false, false, 132), "html", null, true);
            echo "</td>
                  </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID Roles Access</th>
                    <th>Keterangan</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


        </div>
        <!-- /.content-wrapper -->

        <footer class=\"main-footer\">
            <div class=\"float-right d-none d-sm-block\">
                <b>Version</b> 1.1.0
            </div>
            <strong>Copyright &copy; 2022 <a href=\"https://adminlte.io\">DIM BINUS TP - 2 by Ahmad Rijal
                    Hadiyan</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class=\"control-sidebar control-sidebar-dark\">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <!-- Bootstrap 4 -->
    <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>
    <!-- AdminLTE App -->
    
    <script src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/dist/js/adminlte.min.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$(\"#example1\").DataTable({
            \"responsive\": true, \"lengthChange\": false, \"autoWidth\": false,
            \"buttons\": [\"copy\", \"csv\", \"excel\", \"pdf\", \"print\", \"colvis\"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            \$('#example2').DataTable({
            \"paging\": true,
            \"lengthChange\": false,
            \"searching\": false,
            \"ordering\": true,
            \"info\": true,
            \"autoWidth\": false,
            \"responsive\": true,
            });
        });
    </script>
    
</body>


";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "roles/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 188,  299 => 187,  295 => 186,  291 => 185,  287 => 184,  283 => 183,  279 => 182,  273 => 179,  268 => 177,  224 => 135,  215 => 132,  211 => 131,  208 => 130,  204 => 129,  147 => 75,  138 => 69,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Dasboard DIM{% endblock %}

{% block body %}


<body class=\"hold-transition sidebar-mini\">
    <!-- Site wrapper -->
    <div class=\"wrapper\">
        <!-- Navbar -->
        <nav class=\"main-header navbar navbar-expand navbar-white navbar-light\">
            <!-- Left navbar links -->
            <ul class=\"navbar-nav\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\" role=\"button\"><i class=\"fas fa-bars\"></i></a>
                </li>
                <li class=\"nav-item d-none d-sm-inline-block\">
                    <a href=\"../../index3.html\" class=\"nav-link\">Home</a>
                </li>
                <li class=\"nav-item d-none d-sm-inline-block\">
                    <a href=\"#\" class=\"nav-link\">Contact</a>
                </li>
            </ul>

            <!-- Right navbar links -->
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class=\"main-sidebar sidebar-dark-primary elevation-4\">

            <!-- Sidebar -->
            <div class=\"sidebar\">
                <!-- Sidebar user (optional) -->
                <div class=\"user-panel mt-3 pb-3 mb-3 d-flex\">

                </div>

                <!-- SidebarSearch Form -->
                <div class=\"form-inline\">
                    <div class=\"input-group\" data-widget=\"sidebar-search\">
                        <input class=\"form-control form-control-sidebar\" type=\"search\" placeholder=\"Search\"
                            aria-label=\"Search\">
                        <div class=\"input-group-append\">
                            <button class=\"btn btn-sidebar\">
                                <i class=\"fas fa-search fa-fw\"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class=\"mt-2\">
                    <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\"
                        data-accordion=\"false\">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class=\"nav-item\">
                            <a href=\"#\" class=\"nav-link\">
                                <i class=\"nav-icon fas fa-tachometer-alt\"></i>
                                <p>
                                    Konfigurasi
                                    <i class=\"right fas fa-angle-left\"></i>
                                </p>
                            </a>
                            <ul class=\"nav nav-treeview\">
                                <li class=\"nav-item\">
                                    <a href=\"{{'/roles'}}\" class=\"nav-link\">
                                        <i class=\"far fa-circle nav-icon\"></i>
                                        <p>Roles Pengguna</p>
                                    </a>
                                </li>
                                <li class=\"nav-item\">
                                    <a href=\"{{'user'}}\" class=\"nav-link\">
                                        <i class=\"far fa-circle nav-icon\"></i>
                                        <p>Pengguna</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class=\"content-wrapper\">
            <!-- Content Header (Page header) -->
            <section class=\"content-header\">
                <div class=\"container-fluid\">
                    <div class=\"row mb-2\">
                        <div class=\"col-sm-6\">
                            <h1>Roles Pengguna</h1>
                        </div>
                        <div class=\"col-sm-6\">
                            <ol class=\"breadcrumb float-sm-right\">
                                <li class=\"breadcrumb-item\">Roles</li>
                                <li class=\"breadcrumb-item active\">Roles Pengguna</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
    <section class=\"content\">
      <div class=\"container-fluid\">
        <div class=\"row\">
          <div class=\"col-12\">
            <div class=\"card\">
              <div class=\"card-header\">
                <h3 class=\"card-title\">DataTable with minimal features & hover style</h3>
              </div>
              <!-- /.card-header -->
              <div class=\"card-body\">
                <table id=\"example2\" class=\"table table-bordered table-hover\">
                  <thead>
                  <tr>
                    <th>ID Roles Access</th>
                    <th>Keterangan</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                    {% for data in list %}
                  <tr>
                    <td>{{data.id}}</td>
                    <td>{{data.keterangan}}</td>
                  </tr>
                    {% endfor %}
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID Roles Access</th>
                    <th>Keterangan</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


        </div>
        <!-- /.content-wrapper -->

        <footer class=\"main-footer\">
            <div class=\"float-right d-none d-sm-block\">
                <b>Version</b> 1.1.0
            </div>
            <strong>Copyright &copy; 2022 <a href=\"https://adminlte.io\">DIM BINUS TP - 2 by Ahmad Rijal
                    Hadiyan</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class=\"control-sidebar control-sidebar-dark\">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src=\"{{asset('assets/plugins/jquery/jquery.min.js')}}\"></script>
    <!-- Bootstrap 4 -->
    <script src=\"{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}\"></script>
    <!-- AdminLTE App -->
    
    <script src=\"{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}\"></script>
    <script src=\"{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}\"></script>
    <script src=\"{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}\"></script>
    <script src=\"{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}\"></script>
    <script src=\"{{asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}\"></script>
    <script src=\"{{asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}\"></script>
    <script src=\"{{asset('assets/dist/js/adminlte.min.js')}}\"></script>

    <script>
        \$(function () {
            \$(\"#example1\").DataTable({
            \"responsive\": true, \"lengthChange\": false, \"autoWidth\": false,
            \"buttons\": [\"copy\", \"csv\", \"excel\", \"pdf\", \"print\", \"colvis\"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            \$('#example2').DataTable({
            \"paging\": true,
            \"lengthChange\": false,
            \"searching\": false,
            \"ordering\": true,
            \"info\": true,
            \"autoWidth\": false,
            \"responsive\": true,
            });
        });
    </script>
    
</body>


{% endblock %}", "roles/index.html.twig", "/applications/xampp/xamppfiles/htdocs/tp2/templates/roles/index.html.twig");
    }
}
