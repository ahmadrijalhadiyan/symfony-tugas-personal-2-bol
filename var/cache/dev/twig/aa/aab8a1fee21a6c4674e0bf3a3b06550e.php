<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login/index.html.twig */
class __TwigTemplate_ef130be77ba2490e080a968137b5e0a7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "login/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "login/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"login-box\">
  <div class=\"login-logo\">
    <a href=\"../../index2.html\"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class=\"card\">
    <div class=\"card-body login-card-body\">
      <p class=\"login-box-msg\">Sign in to start your session</p>

      <form action=\"../../index3.html\" method=\"post\">
        <div class=\"input-group mb-3\">
          <input type=\"email\" class=\"form-control\" placeholder=\"Email\">
          <div class=\"input-group-append\">
            <div class=\"input-group-text\">
              <span class=\"fas fa-envelope\"></span>
            </div>
          </div>
        </div>
        <div class=\"input-group mb-3\">
          <input type=\"password\" class=\"form-control\" placeholder=\"Password\">
          <div class=\"input-group-append\">
            <div class=\"input-group-text\">
              <span class=\"fas fa-lock\"></span>
            </div>
          </div>
        </div>
        <div class=\"row\">
          <div class=\"col-8\">
            <div class=\"icheck-primary\">
              <input type=\"checkbox\" id=\"remember\">
              <label for=\"remember\">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class=\"col-4\">
            <button type=\"submit\" class=\"btn btn-primary btn-block\">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class=\"social-auth-links text-center mb-3\">
        <p>- OR -</p>
        <a href=\"#\" class=\"btn btn-block btn-primary\">
          <i class=\"fab fa-facebook mr-2\"></i> Sign in using Facebook
        </a>
        <a href=\"#\" class=\"btn btn-block btn-danger\">
          <i class=\"fab fa-google-plus mr-2\"></i> Sign in using Google+
        </a>
      </div>
      <!-- /.social-auth-links -->

      <p class=\"mb-1\">
        <a href=\"forgot-password.html\">I forgot my password</a>
      </p>
      <p class=\"mb-0\">
        <a href=\"register.html\" class=\"text-center\">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "login/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"login-box\">
  <div class=\"login-logo\">
    <a href=\"../../index2.html\"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class=\"card\">
    <div class=\"card-body login-card-body\">
      <p class=\"login-box-msg\">Sign in to start your session</p>

      <form action=\"../../index3.html\" method=\"post\">
        <div class=\"input-group mb-3\">
          <input type=\"email\" class=\"form-control\" placeholder=\"Email\">
          <div class=\"input-group-append\">
            <div class=\"input-group-text\">
              <span class=\"fas fa-envelope\"></span>
            </div>
          </div>
        </div>
        <div class=\"input-group mb-3\">
          <input type=\"password\" class=\"form-control\" placeholder=\"Password\">
          <div class=\"input-group-append\">
            <div class=\"input-group-text\">
              <span class=\"fas fa-lock\"></span>
            </div>
          </div>
        </div>
        <div class=\"row\">
          <div class=\"col-8\">
            <div class=\"icheck-primary\">
              <input type=\"checkbox\" id=\"remember\">
              <label for=\"remember\">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class=\"col-4\">
            <button type=\"submit\" class=\"btn btn-primary btn-block\">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class=\"social-auth-links text-center mb-3\">
        <p>- OR -</p>
        <a href=\"#\" class=\"btn btn-block btn-primary\">
          <i class=\"fab fa-facebook mr-2\"></i> Sign in using Facebook
        </a>
        <a href=\"#\" class=\"btn btn-block btn-danger\">
          <i class=\"fab fa-google-plus mr-2\"></i> Sign in using Google+
        </a>
      </div>
      <!-- /.social-auth-links -->

      <p class=\"mb-1\">
        <a href=\"forgot-password.html\">I forgot my password</a>
      </p>
      <p class=\"mb-0\">
        <a href=\"register.html\" class=\"text-center\">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
{% endblock %}
", "login/index.html.twig", "/applications/xampp/xamppfiles/htdocs/tp2/templates/login/index.html.twig");
    }
}
