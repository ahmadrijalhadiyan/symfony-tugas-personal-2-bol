<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login/index.html.twig */
class __TwigTemplate_18d6826ee55813f0574917756c63b890 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "login/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "login/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Login DIM";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<body class=\"hold-transition login-page\">
    <div class=\"login-box\">
        <div class=\"login-logo\">
            <a href=\"#\"><b>Aplikasi - </b>DIM</a>
        </div>
        <!-- /.login-logo -->
        <div class=\"card\">
            <div class=\"card-body login-card-body\">
                <p class=\"login-box-msg\">Aplikasi Data Information Management</p>

                <form action=\"../../index3.html\" method=\"post\">
                    <div class=\"input-group mb-3\">
                        <input type=\"email\" class=\"form-control\" placeholder=\"Email\" name=\"email\" required>
                        <div class=\"input-group-append\">
                            <div class=\"input-group-text\">
                                <span class=\"fas fa-envelope\"></span>
                            </div>
                        </div>
                    </div>
                    <div class=\"input-group mb-3\">
                        <input name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" required>
                        <div class=\"input-group-append\">
                            <div class=\"input-group-text\">
                                <span class=\"fas fa-lock\"></span>
                            </div>
                        </div>
                    </div>
                    <div class=\"input-group mb-3\">
                        <select name=\"role\" class=\"form-control\" required>
                            <option readonly >Pilih Jenis Roles Anda</option>
                            <option value=\"1\">Pengguna</option>
                            <option value=\"2\">Admin</option>
                            <option value=\"3\">Super Admin</option>
                        </select>
                        <div class=\"input-group-text\">
                            <span class=\"fas fa-cog\"></span>
                        </div>
                    </div>
                    <div class=\"row\">
                        <!-- /.col -->
                        <div class=\"col-12\">
                            <button type=\"submit\" class=\"btn btn-primary btn-block\">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>


                <p class=\"mb-1\">
                    <a href=\"#\">Lupa Password</a>
                </p>
                <p class=\"mb-0\">
                    <a href=\"#\" class=\"text-center\">Buat Member Baru</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <!-- Bootstrap 4 -->
    <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>
    <!-- AdminLTE App -->
    <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/dist/js/adminlte.min.js"), "html", null, true);
        echo "\"></script>
</body>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "login/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 71,  141 => 69,  136 => 67,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Login DIM{% endblock %}

{% block body %}
<body class=\"hold-transition login-page\">
    <div class=\"login-box\">
        <div class=\"login-logo\">
            <a href=\"#\"><b>Aplikasi - </b>DIM</a>
        </div>
        <!-- /.login-logo -->
        <div class=\"card\">
            <div class=\"card-body login-card-body\">
                <p class=\"login-box-msg\">Aplikasi Data Information Management</p>

                <form action=\"../../index3.html\" method=\"post\">
                    <div class=\"input-group mb-3\">
                        <input type=\"email\" class=\"form-control\" placeholder=\"Email\" name=\"email\" required>
                        <div class=\"input-group-append\">
                            <div class=\"input-group-text\">
                                <span class=\"fas fa-envelope\"></span>
                            </div>
                        </div>
                    </div>
                    <div class=\"input-group mb-3\">
                        <input name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" required>
                        <div class=\"input-group-append\">
                            <div class=\"input-group-text\">
                                <span class=\"fas fa-lock\"></span>
                            </div>
                        </div>
                    </div>
                    <div class=\"input-group mb-3\">
                        <select name=\"role\" class=\"form-control\" required>
                            <option readonly >Pilih Jenis Roles Anda</option>
                            <option value=\"1\">Pengguna</option>
                            <option value=\"2\">Admin</option>
                            <option value=\"3\">Super Admin</option>
                        </select>
                        <div class=\"input-group-text\">
                            <span class=\"fas fa-cog\"></span>
                        </div>
                    </div>
                    <div class=\"row\">
                        <!-- /.col -->
                        <div class=\"col-12\">
                            <button type=\"submit\" class=\"btn btn-primary btn-block\">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>


                <p class=\"mb-1\">
                    <a href=\"#\">Lupa Password</a>
                </p>
                <p class=\"mb-0\">
                    <a href=\"#\" class=\"text-center\">Buat Member Baru</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src=\"{{asset('assets/plugins/jquery/jquery.min.js')}}\"></script>
    <!-- Bootstrap 4 -->
    <script src=\"{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}\"></script>
    <!-- AdminLTE App -->
    <script src=\"{{asset('assets/dist/js/adminlte.min.js')}}\"></script>
</body>
{% endblock %}
", "login/index.html.twig", "/applications/xampp/xamppfiles/htdocs/tp2/templates/login/index.html.twig");
    }
}
